# Welcome to Linux InstallFest!

![Tux](images/tux.png)

Welcome to our guide on getting Linux up and running!

This is a step-by-step guide, with detailed explanations on how to install Linux, start with the [README](./disclaimer.md) section, then check out the Install Methods page to find which way of installing Linux suits you best, make sure you carry out the pre-install checks and away you go!

*If you're new to Linux and this is your first time installing it, we would 
recommend you go for an Ubuntu dual boot setup. Ubuntu is more stable and 
a dual boot will help you learn the Linux environment, whilst keeping your 
Windows install intact.*

This isn't meant to be an entirely standalone guide, however, it should be a
pretty good collection of our understanding of how to install Linux. If
you're around on Wednesday 30th of September, then make sure to join us on
[our discord](https://discord.com/invite/EkcRZc4), so we can help you out!

**How the Discord setup will work**:

There is a set of Voice Chats and text chats in the AFNOM discord. **Please
join the one corresponding to the install method you are interested in.**

![Discord channels](./images/discord-chats.png)

The role `@install-helpers` can be used to draw the attention of those who will
be helping you on the day. **Please rememeber that everyone who is helping is a volunteer.**
We will **not** accept rude or offensive behaviour to others, nor harassment or 
discrimination against others.

The helpers will do their best to help you out if you encounter any problem.
This guide should be your first point of contact. If you **read** the pages
of your chosen install method (*please do not skip any, they are there to help __you__*), 
you should be able to successfully install your chosen OS without problems.

You will need to have a phone/tablet to read the guide.

<span style="color:red">Don't forget to <strong>backup</strong> any important data 
you don't want to lose!</span>

Enjoy and welcome to the world of Linux! &#129395;