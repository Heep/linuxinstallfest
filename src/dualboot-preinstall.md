# Before proceeding with installation

<span style="color:red; font-weight:bold"> IF YOU HAVE BITLOCKER ENABLED ON
YOUR WINDOWS, PLEASE DISABLE IT BEFORE PROCEEDING ANY FURTHER. If you fail to
disable it, your Windows may <em>fail to boot</em> after disabling Secure
Boot. If this happens, your only chance is if you have a recovery key in your
Microsoft account. Just disable it while you are doing the installation, you
can enable it afterwards!</span>

Also, please don't forget to make a **backup** of any important data!

## Making space for the Linux partition

To dualboot you need to create a new area for Linux to live on. This is called a **Partition**, and it's a virtual subdivision of your physical hard drive. So when dualbooting the first thing to do is to, from Windows, partition your hard drive. This means shrinking the amount of your drive that Windows owns so we can give some to Linux, so we're basically going to be cutting the drive into parts.

From Windows, open the Disk Management utility. Select your Windows partition
(usually `C:\`), right click and select Shrink.

![Windows partitions](./images/partitions1.png)

Enter the amount of space to shrink. This is how much you want to allocate for your Linux partition. Remember, Ubuntu recommends at least 25GB and Manjaro 30GB. We'd recomend allocating as much space as you feel comfortable with if you intend to be using Linux a lot. **Make sure you don't leave Windows with less space than it says it's using: otherwise you'll be deleting data**.

![Windows partitions](./images/partitions2.png)

You can use [this tool](https://www.gbmb.org/gb-to-mb) to convert GB to MB values.

Once you click Shrink, the Windows partition will be downsized and you should see an 
**Unallocated** chunck in the Disk management utility.

![Windows partitions](./images/partitions3.png)

**Note**: You may only be able to shrink the Windows partition to half its size, even if you have 
more than half the partition free. This is because Windows places some files **bang in the middle** of its
partition. Disable Hybernation by running a command line as Administrator and typing:

    powercfg /h off

Disable paging for the drive C by going to Control Panel -> System and Security -> System.
Select Advanced System Settings from the left side menu.

![Control panel advanced](./images/control-panel-advanced.png)

Click the Settings button of the Performance category, and switch to the Advanced tab.
Click the change button under Virtual Memory. Uncheck Automatically manage paging files
and select no paging file for the drive. Click Okay and reboot when prompted.

![Remove paging](./images/rm-paging.png)

When you're back to Windows, you can now retry shrinking the partition. It should work just fine.

## What is the BIOS and why do we care?

The BIOS is the part of your computer responsible for managing hardware and
booting other operating systems.

![BIOS diagram](images/bios_diagram.png)

The BIOS is where we configure some of the lowest level hardware to make sure
it's compatible before we mess around with our operating systems, so we're
going to need to interact with it when preparing to dualboot.

Since the vast majority of laptops come with windows preinstalled, their BIOSs
are often preconfigured with settings developed explicitly for windows.
Therefore when dualbooting some if these settings can be incompatible with
installing linux and we need to tweak them. 

## How to access the BIOS

The BIOS is only really accessible when you boot your PC so you can't adjust its settings while running an OS. You're going to have to restart your device.
There are 2 ways to access the BIOS: you can trigger it from Windows *OR* restart your computer and enter manually.

The way its entered manually is a bit manufacturer specific in terms of which key to press whereas the Windows method is longer but more standardised.

### From Windows:

1: Open **Settings**  
2: Go to **Update & Security**  
3: Open the **Recovery** tab  
4: Under "Advanced Settings" restart your device using the **Restart Now** button  
5: When your device restarts and you're presented with a menu select **Troubleshoot**  
6: Select **Advanced Options**  
7: Select **UEFI Firmware settings**  
8: Click **Restart**  

Your device will now restart and take you straight to the BIOS.

### Manually:

Restart your device. Before you enter windows you will see an icon of your laptop manyufacturer e.g Lenovo, Dell etc.

At this screen press the f1 key and you *should* be brought to the BIOS.

If not don't worry it's just that the manufacturer has changed the button to press.

There might also be a message, for exmaple Lenovo shows: "To interrupt normal startup press Enter". Press the key it says and you should be brought to a menu with several options, one of which will either say "adjust BIOS settings" or something similar, use the arrow keys to select this and press Enter.

## How to do stuff in the BIOS

The BIOS is very low level, hence why it looks simple and works simply. You can move around the menu with the arrow keys, maybe the Tab key and selecting submenus using the Enter key, and perhaps readjusting the ordering using the +/- keys.

Hopefully it will have a handy guide of which keys do what, but if in doubt google how to navigate your manufacturers BIOS.

It's a bit difficult to give a general guide since the BIOS is specific to every manufacturers motherboard but it will have all the settings you need *somewhere* and hopefully organised fairly logically.

In general they have several tabs, each with a different category of settings, you use the arrow keys to move selection, the enter key to change things. There will probably be a BOOT/Startup tab that's important to us.

It's important that you make sure to **SAVE** the settings before leaving the BIOS, make sure when you exit you select whichever option is "Save and Exit" not just "Exit". Otherwise, guess what, all the changes you made won't stick.

Here are a few common things you should configure in the BIOS before going ahead:

## Boot order

Boot order is very important to actually be able to boot to the installation
media, like a USB!

By default, the system will try and boot off the first hard drive/solid state
drive available, however, modern computers can boot off of many different
media types, including network, cd drives, usb drives, etc.

In your BIOS, locate a section for "Boot Order", and rearrange them to place
USB at the very top, before any other method. It's possible that your BIOS
doesn't have this setting - which is very sad. If it doesn't, then when you
try to boot your installation media, you need to interrupt the boot process
and manually select the USB drive.

## Secure boot

We're going to need to disable this, but don't worry it won't really make
your computer insecure!

Basically the idea of secure boot is that when booting into an operating
system the BIOS checks against a *cryptographic signature*. The issue here is
that whilst these signatures will work for windows, it won't for *all* linux
versions. It might work for some (Ubuntu 20), but having it will also prevent
you running virtual machines (which you'll really want). So to install linux
we want to disable it.

[What Is Secure Boot? How to Enable and Disable It in Windows?](https://www.minitool.com/lib/secure-boot.html)

You'll find this under the BOOT tab. Switch it to **Disabled**.

## Fast Boot

Fast Boot is a Windows-only technology that enables faster booting by instead
of "properly" shutting down, enters a state similar to hibernation which
allows faster loading of the Windows kernel and drivers. Disabling Fast Boot
will make your Windows boot times *slightly* slower, but probably only by a
few seconds at worst.

However, it's completely incompatible with Linux, so you'll want to disable
it in your BIOS. **Before** doing this, disable fast startup in Windows, and
perform a full shutdown before rebooting into the BIOS.

![Fast Startup](images/fast-startup-windows.png)

Then, you can set Fast Boot to **Disabled**, usually found under the BOOT
tab.

## Intel RST

You *may or may not* need to do this step. Please read the following paragraphs
to understand what RST is and to learn how to identify whether you will need it.

Intel Rapid Storage Technology can generally speed up your system and make it
feel more responsive, however, because of a lack of drivers on Linux, it's
quite common that your installer *will not detect* your hard drive if you
have it enabled.

You can disable it, usually, by modifying your hard drive to use plain AHCI
instead of Intel Optane in the "Devices" section of the BIOS. However, before
you do this, be aware that you may take a small performance impact and notice
your hard drive speeds a bit slower than before. Also, note that if you're
planning on keeping Windows around, you'll need to do some prep before
making the change.

The Ubuntu installer will prompt you if changes to Intel RST are required. 
It's easy to determine, even for Manjaro, as you won't see your disk in 
the installer list, when you reach the partitioning step in the guide.
If this happens, come back here and follow the guide below.

To avoid the chance of something going wrong, follow the guide on the Ubuntu
discourse [here](https://discourse.ubuntu.com/t/ubuntu-installation-on-computers-with-intel-r-rst-enabled/15347),
taking special note of the required instructions on the Windows side of
things.
