# Installing Ubuntu in a Virtual Machine

This guide will help you install Ubuntu in a Virtual Machine. It is written
under the assumption you are using Windows. The VirtualBox installation for
MacOS should be pretty much the same. For Linux distributions, install
VirtualBox through your package manager.

You will need:
* [The Ubuntu ISO](https://ubuntu.com/download/desktop)

This guide assumes you have VirtualBox installed. If you don't, it means
you skipped the [Virtual Machine Pre-install](./../../vm-preinstall.md) post!
Head back there and follow those steps.

## Installing Ubuntu

In VirtualBox, go to Machine -> New (or just press `CTRL+N`). Input the name
you want for your VM (good practice is to include what OS it is), select the
type as `Linux` and the version as `Ubuntu (64-bit)`.

![../../images/ubuntu/vm-create-os.png](../../images/ubuntu/vm-create-os.png)

Next, you'll be asked to select the memory size. This depends on what specs
**your** machine has. Ubuntu recommends a minimum of 4 GB RAM (4096 MB). You
can allocate it more, but don't worry too much, these settings can be
modified later too!

![../../images/ubuntu/vm-create-mem.png](../../images/ubuntu/vm-create-mem.png)

We are then going to create a virtual hard disk now (default option), and the
Hard disk file type should be VDI (VirtualBox Disk Image). Choose to have the
disk dynamically allocated. Next, choose the size of the hard disk. Ubuntu
recommends at least 25GB.

![../../images/ubuntu/vm-create-hdd.png](../../images/ubuntu/vm-create-hdd.png)

Done! You should now see your new VM in the list:

![../../images/ubuntu/vm-created.png](../../images/ubuntu/vm-created.png)

Selecting the new VM (called Ubuntu in this example), click Settings (or
press `CTRL+S`) and go to the Storage option. Under `Controller: IDE`, click
the `Empty` disk. Click the blue disk next to the Optical Drive dropdown, and
select `Choose a disk file`. Navigate to where you saved the Ubuntu ISO and
open it. 

![../../images/ubuntu/vm-insert-iso.png](../../images/ubuntu/vm-insert-iso.png)



Back to the list of VMs, select the Ubuntu VM and press Start. Your VM
should start. You may be asked to confirm the startup disk, select the
Ubuntu ISO and press Start.


**Note**: If your VM gets stuck in a boot loop and the error message is related
to **VMSVGA**, you need to change the graphics controller. In Settings, go to the 
Display option and **change** the graphics controller to be `VBoxSVGA`. 

![../../images/ubuntu/vm-display-settings.png](../../images/ubuntu/vm-display-settings.png)

Back to the VM booting -- the Ubuntu installer should greet you:

![../../images/ubuntu/vm-ubuntu-installer.png](../../images/ubuntu/vm-ubuntu-installer.png)

Click "Install Ubuntu" and follow the steps through.

## Select your language

![Installer Language](../../images/ubuntu/installer-language.png)

## Select your keyboard layout

The auto-detect keyboard should walk you through finding out exactly what
layout you have if you're not sure.

![Installer Keyboard](../../images/ubuntu/installer-keyboard.png)

## Select software

In most cases you want a "Normal installation" with all the utilities -
however, if you're working with less disk space, or want to manually install
only the tools you want later, then go with a "Minimal installation".

If you have an internet connection, then select "Download updates" - it makes
the install process a little longer, but ensures that everything will be
properly up to date.

The "Install third-party software" is slightly more complex. In most cases,
you should tick it, and attempt an install - if something breaks and doesn't
work, for issues related to drivers, then you can try again, disabling this
step, and instead trying to install the drivers and codecs after the install
is fully complete.

![Installer Software](../../images/ubuntu/installer-software.png)

## Choose installation type

**Be careful at this step! After you click "Install Now" the install process
will begin!**

For a pure install, click "Erase disk and install Ubuntu".

![Installer Type](../../images/ubuntu/installer-type.png)

At this step, you can also choose to encrypt your disk, by selecting
"Advanced features" and selecting both "Use LVM" and "Encrypt the new Ubuntu
installation". However, this is optional, and requires you to input your
password at each boot.

![Installer Encrypt drives](../../images/ubuntu/installer-encrypt.png)

After you click "Install Now", you'll be asked if you want to proceed with the partitioning layout you've selected:

![Installer Partitioning](../../images/ubuntu/installer-partitioning.png)

Verify the changes, and then click "Continue". Note that your partitions
*will* look different depending on your setup.

## Select your timezone

![Installer Location](../../images/ubuntu/installer-location.png)

## Setup your account

You need to pick:

- Your name (used in the display manager to greet you, etc)
- Your computer's name (the hostname used on networks, pick something unique and recognizable)
- Your username (used to login, appears in shell prompts, etc)
- Your password (standard password guidelines apply, if you want something easy to remember and secure, try [diceware](https://en.wikipedia.org/wiki/Diceware))

![Installer Account](../../images/ubuntu/installer-whoami.png)

## Wait!

Now just wait for the installer to complete!

![Installer Wait](../../images/ubuntu/installer-wait.png)

Enjoy your new Ubuntu VM!

Now head over to the [Post Installation](../../post-install.md) guide to update your
system and install some useful software and the [VM Post Install](../../vm-post-install-ga.md)
guide to learn of some useful features for your VM!
